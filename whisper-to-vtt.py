#!/usr/bin/env python3
from sys import argv
from math import floor
if len(argv) != 2:
	print("Usage: TODO")
	exit(0)

try:
	f = open(argv[1], 'r')
	lines = f.readlines()
	f.close()
except:
	print("Couldn't open")
	exit(0)

def to_ms(ts):
	s = ts.replace(":",".").split(".")
	if len(s) == 3:
		return int(s[0])*1000*60 + int(s[1])*1000 + int(s[2])
	elif len(s) == 4:
		return int(s[0])*1000*60*60 + int(s[1])*1000*60 + int(s[2])*1000 + int(s[3]) # untested
def add_time(ts): # TODO doesn't support >1hour
	ms = to_ms(ts)
	# TODO, add cli argument
	# this adds an offset to the subs
	# ms += x*1000
	millis = ms%1000
	seconds = floor(((ms-millis)%(1000*60))/1000)
	minutes = floor(ms/1000/60)
	return str(minutes).zfill(2) + ":" + str(seconds).zfill(2) + "." + str(millis).zfill(3)


print("WEBVTT")
for line in lines:
	ts = line[line.find("[")+1:line.find("]")].split(" --> ")
	if len(ts) != 2:
		continue
	text = line[line.find("]")+3:-1]
	
	start = add_time(ts[0])
	end = add_time(ts[1])
	
	
	print(start + " --> " + end)
	print(text+"\n")
