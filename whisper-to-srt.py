#!/usr/bin/env python3
from sys import argv
if len(argv) != 2:
	print("Usage: whisper-to-srt.py ./whisperout.txt")
	exit(0)

try:
	f = open(argv[1], 'r')
	lines = f.readlines()
	f.close()
except:
	print("Couldn't open file")
	exit(0)
index = 1
for line in lines:
	ts = line[line.find("[")+1:line.find("]")].split(" --> ")
	if len(ts) != 2:
		continue
	ts[0] = ts[0].replace(".",",")
	ts[1] = ts[1].replace(".",",")
	# hack, idk what whisper prints for files >60 minutes
	ts[0] = "00:"+ts[0]
	ts[1] = "00:"+ts[1]
	text = line[line.find("]")+3:-1]
	print(index)
	print(ts[0] + " --> " + ts[1])
	print(text+"\n")
	index += 1